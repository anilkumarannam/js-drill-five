const problem4 = require('./problem4.js');

function problem5(inventory) {

    const carYearList = problem4(inventory);
    let yearListSize = carYearList.length;
    let carList = [];
    
    for (let yearListIndex = 0; yearListIndex < yearListSize; yearListIndex++) {
        let car = inventory[yearListIndex];
        let year = carYearList[yearListIndex];
        if (year < 2000) {
            carList.push(car);
        }
    }
    return carList;
}

module.exports = problem5;