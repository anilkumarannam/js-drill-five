function problem1(inventory, carId) {

    if (inventory !== undefined || inventory !== null || carId !== undefined) {

        let inventorySize = inventory.length;
        for (let index = 0; index < inventorySize; index++) {
            let car = inventory[index];
            if (car.id === carId) {
                let outPutString = "Car " + carId + " is a " + car.car_make + " car " + car.car_year + " " + car.car_make + " " + car.car_model;
                console.log(outPutString);
                break;
            }
        }
    }
}

module.exports = problem1;