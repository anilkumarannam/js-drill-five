function problem4(inventory) {

    if (inventory !== undefined || inventory !== null) {
        let carYearList = [];
        let inventorySize = inventory.length;
        for (let index = 0; index < inventorySize; index++) {
            let car = inventory[index];
            carYearList.push(car.car_year)
        }
        return carYearList
    }
}

module.exports = problem4;